## TypeScript Apollo Server, Prisma ORM

1. Ejecutar `npm i`, instalar las dependencias
2. Transpilar el proyecto con `npm run dev`
3. Migrar los modelos con prisma `npm run migrate`
4. Lanzar el proyecto con `npm run start`

## Ejemplo de ejecuciones con Apollo Server

![Ejemplo de Grahpql](./gql.jpeg)
