import { ApolloServer } from 'apollo-server';
import { ApolloServerPluginInlineTrace } from 'apollo-server-core';
import { PubSub } from 'graphql-subscriptions';

import { enviroment } from './enviroment';

import typeDefs from './graphql/typeDefs.gql';
import resolvers from './graphql/resolvers';

const pubSub = new PubSub();

const server = new ApolloServer({
  typeDefs,
  resolvers,
  cors: true,
  introspection: enviroment.apollo.introspection,
  context: (ctx) => ({ ...ctx, pubSub }),
  plugins: [ApolloServerPluginInlineTrace()],
});

server
  .listen(enviroment.port)
  .then(({ url }) => console.log(`�� Server ready at ${url}`));

(() => {
  if (module.hot) {
    module.hot.accept();
    module.hot.dispose(() => console.log('Module Disposed'));
  }
})();
