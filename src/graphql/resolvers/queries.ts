import { prisma } from 'src/utils';

function testMessage() {
  return { content: 'Apollos' };
}

async function getPosts() {
  return await prisma.post.findMany({
    orderBy: {
      createdAt: 'desc',
    },
  });
}

export default {
  testMessage,
  getPosts,
};
