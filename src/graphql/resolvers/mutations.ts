import { PostInput } from 'gql-inputs';
import { prisma } from 'src/utils';

async function createPost(_, args) {
  let post = args.post as PostInput;
  let postCreated = await prisma.post.create({
    data: {
      title: post.title,
      content: post.content,
    },
  });

  return postCreated;
}

export default {
  createPost,
};
