interface Enviroment {
  apollo: {
    introspection: boolean;
    playground: boolean;
  };
  port: number | string;
}

const defaultPort = 4955;
export const enviroment: Enviroment = {
  apollo: {
    introspection: process.env.APOLLO_INSTROSPECTION === 'true',
    playground: process.env.APOLLO_PLAYGROUND === 'true',
  },
  port: process.env.PORT || defaultPort,
};
