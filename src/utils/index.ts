import { PrismaClient } from '@prisma/client';

export const prisma = new PrismaClient();

(async () => {
  await prisma.$disconnect();
})();
