declare module 'models-app' {
  export type Post = {
    id: string | number;
    createdAt: string;
    updatedAt: string;
    title: string;
    content: string;
  };
}
