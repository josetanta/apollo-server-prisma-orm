declare module '*.gql' {
  import { DocumentNode } from 'graphql';
  const value: DocumentNode;
  export = value;
}
declare module '*.graphql' {
  import { DocumentNode } from 'graphql';
  const value: DocumentNode;
  export = value;
}

declare module 'gql-inputs' {
  export type PostInput = {
    title: string;
    content: string;
  };
}
