const path = require('path');
const webpack = require('webpack');
require('dotenv').config({ path: path.resolve(__dirname, '.env') });

const tsconfig = require('./tsconfig.json');

module.exports = {
  module: {
    rules: [
      {
        test: /\.(m?js|ts)$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
            plugins: ['@babel/plugin-transform-runtime'],
          },
        },
      },
      {
        test: /.ts$/,
        exclude: /node_modules/i,
        use: 'ts-loader',
      },
    ],
  },
  output: {
    filename: 'server.js',
    path: path.resolve(__dirname, 'dist'),
  },
  resolve: {
    extensions: ['.ts', '.js'],
    modules: [
      path.resolve(__dirname, 'node_modules'),
      path.resolve(__dirname, tsconfig.compilerOptions.baseUrl),
    ],
  },
  target: 'node',
  plugins: [new webpack.ProgressPlugin()],
};
